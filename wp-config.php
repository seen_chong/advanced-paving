<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'adv-pave_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Increase Memory - Jay 03-24-15 */
define( 'WP_MEMORY_LIMIT', '96M' );

/** Disable Cron. Enable in cpanel - Jay 03-24-15 */
define('DISABLE_WP_CRON', true);

define('WP_HOME','http://advanced-paving.dev/');
define('WP_SITEURL','http://advanced-paving.dev/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
// define('AUTH_KEY',         'zEo&y_aJNLt%:vMSlXxUrr>3HLBfPx5wA+bJ>1C~xOzu|-}&1Vym,<N4jR1xP*V5');
// define('SECURE_AUTH_KEY',  '/!DYiX,-*6#u_d0_%W]a2Qo+fOZ,BgYgGU~|OlK51!p6b-3L9I)t#BoXV&n0c-s0');
// define('LOGGED_IN_KEY',    ' 6(htol1G||&.JZDM=>JOJ]|kR[U<)Kek~w3F7L{|`KaMJeTI:JOLCrD5sC&o#`&');
// define('NONCE_KEY',        '@LD+[O>rhTY_-PYT%u%4jJtA,V2YzLqT+(khgvca%#-|(c3GfH+sik`Vmtf2Nw!p');
// define('AUTH_SALT',        '()ckQ*f^O2q{+:WjW}> aJ/Fv&+O*k*&t#xPG){3P$,^<jl,Ws_5et;^CVSQBLFJ');
// define('SECURE_AUTH_SALT', '-FZQ)J(a{:2bo}!WI-k@l*IRR&R6C@^pUYkX|T4v =!]t/{KBY^:%w+y[vfP2$qh');
// define('LOGGED_IN_SALT',   '3:BT7V;{(-`6|{P2# tCr*UuphU7ubvzcxtu2@/}_+Cf^zR1Aov+FMv)7kR@39?2');
// define('NONCE_SALT',       '@uN#4f3wp^Oh+8k]T,tbSL1+[hA&_#p/[.18N/l;.I0X}@X5*pu}Oz-9-=E_$#u2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
